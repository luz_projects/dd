package elkware.cityguide.com.onemyguide.Adarpters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import elkware.cityguide.com.onemyguide.Daos.Places;
import elkware.cityguide.com.onemyguide.DetailsActivities.Places_Activity;
import elkware.cityguide.com.onemyguide.OnLoadMoreListener;
import elkware.cityguide.com.onemyguide.R;

public class Places_Adapter extends RecyclerView.Adapter<Places_Adapter.MyPlaceHolder> {
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    Context mContext;
    List<Places> mPlaces;

    public Places_Adapter(Context mContext, List<Places> mPlaces, RecyclerView recyclerView) {
        this.mContext = mContext;
        this.mPlaces = mPlaces;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mPlaces.get(position)!=null ? 1: 0;
    }

    @Override
    public MyPlaceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyPlaceHolder placeHolder = null;

        if (viewType==1){
            View v = LayoutInflater.from(mContext).inflate(R.layout.items_places_to_visite, parent, false);
            placeHolder = new MyPlaceHolder(v);
        } else{
            View v = LayoutInflater.from(mContext).inflate(R.layout.layout_loadin_item,parent, false);
            placeHolder = new  ProgressViewHolder(v);
        }


        return placeHolder;

    }

    @Override
    public void onBindViewHolder(Places_Adapter.MyPlaceHolder holder, final int position) {
        if (holder instanceof MyPlaceHolder){
            ((MyPlaceHolder)holder).place_name.setText(mPlaces.get(position).getPlace_name());
            ((MyPlaceHolder)holder).place_adress.setText(mPlaces.get(position).getPlace_adresse());
            ((MyPlaceHolder)holder).place_image.setImageResource(mPlaces.get(position).getPlace_image());

            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, Places_Activity.class);
                    intent.putExtra("place_name", mPlaces.get(position).getPlace_name());
                    intent.putExtra("place_adress", mPlaces.get(position).getPlace_adresse());
                    intent.putExtra("place_descrip", mPlaces.get(position).getPlace_descrip());
                    intent.putExtra("place_image", mPlaces.get(position).getPlace_image());
                    mContext.startActivity(intent);
                }
            });
            holder.fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Add to Favourite", Toast.LENGTH_SHORT).show();
                }
            });

            holder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Share Place", Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }



    }


    public void setLoad(){
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener){
        this.onLoadMoreListener = onLoadMoreListener;
    }
    public interface OnLoadMoreListener {
        void onLoadMore();
    }
    public void setLoaded() {
        loading = false;
    }


    @Override
    public int getItemCount() {
        return mPlaces.size();
    }



    public class MyPlaceHolder extends RecyclerView.ViewHolder {
        TextView place_name;
        TextView place_adress;
        TextView place_descrip;
        ImageView place_image;
        LinearLayout container;
        ImageView share, fav;

        public MyPlaceHolder(View itemView) {
            super(itemView);
            place_name = itemView.findViewById(R.id.place_name);
            place_adress = itemView.findViewById(R.id.place_adress);
            place_descrip = itemView.findViewById(R.id.place_descrip);
            place_image = itemView.findViewById(R.id.place_image);
            container = itemView.findViewById(R.id.container);
            fav = itemView.findViewById(R.id.ad_favourite);
            share = itemView.findViewById(R.id.share_place);


        }
    }


    public class ProgressViewHolder extends MyPlaceHolder{
        public ProgressBar progressBar;
        public ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar1);
        }
    }
}
