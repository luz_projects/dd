package elkware.cityguide.com.onemyguide.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import elkware.cityguide.com.onemyguide.Adarpters.Places_Adapter;
import elkware.cityguide.com.onemyguide.Daos.Places;
import elkware.cityguide.com.onemyguide.R;

public class Places_to_Visite extends Fragment {
    View rootView;
    RecyclerView recyclerView;
    List<Places> placesList;
    private Handler handler;

    public Places_to_Visite() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
placesList = new ArrayList<>();
        placesList.add(new Places(
                "Glasshurst",
                "Fleming 196 Woodside Circle Mobile",
                R.drawable.eiffeltower,
                ""
        ));
        placesList.add(new Places(
                "Tour Eiffel",
                "Days 3756 Preston Street Wichita",
                R.drawable.eiffeltower_2,
                "La Tour de la France"
        ));
        placesList.add(new Places(
                "Corhall",
                "Stephens 1635 Franklin Street Montgomery",
                R.drawable.bodywater,
                "La Tour de la France"
        ));
        placesList.add(new Places(
                "Westermont",
                "Palmer 2595 Pearlman Avenue Sudbury",
                R.drawable.pari,
                "La Tour de la France"
        ));
        placesList.add(new Places(
                "Rockpond",
                "Porterfield 508 Virginia Street Chicago",
                R.drawable.pariscentre,
                "La Tour de la France"
        ));
        placesList.add(new Places(
                "Morpond",
                "Flores 1516 Holt Street West Palm Beach",
                R.drawable.pariscentre,
                "La Tour de la France"
        ));

    }

    public void addToFavourite(View view) {
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_place_tovisite, container, false);
        recyclerView = rootView.findViewById(R.id.place_recycleView);
        final Places_Adapter places_adapter = new Places_Adapter(getContext(), placesList, recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(places_adapter);
        recyclerView.setHasFixedSize(true);
        return rootView;

    }
}